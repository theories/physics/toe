# theories/physics/toe

## About this project

The purpose of this project is to describe a theory of everything for nature. The nature is the only source of truth. If you have a good candidate to be this theory send an issue in project [**candidates**](https://gitlab.com/theories/physics/candidates-top). But before, what's the outlines of a theory of everything ?

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Licenses

Any project content in this project is under [**Creative Commons LICENSE**](LICENSE.txt) and any project code content in this project is under [**MIT LICENSE**](LICENSE-CODE.txt).

## Nature as box

A theory of nature is a set of equations, algorithms or another ways, describing the behavior of nature. Given an experience as input, the theory gives you the output. We can imagine the nature as box with input and output. The theory of everything must describe the behavior of this box. Output is a set of pairs, a pair contains the value and its probability. This set can be continuous ou discrete, finite or infinite. The set of output values is called output **spectrum**.

![nature as box](content/README/nature-as-box.png)

If the theory gives a unique output (probability = 1), it's called **deterministic** theory, otherwise it's **probabilistic** theory.